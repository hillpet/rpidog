#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi watchdog for misc purposes
#
#	Copyright (c) 2012-2024 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),1)
#
# RPi#1 (Rev-1): Metal case with CAM
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="spicam.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),2)
#
# RPi#2 (Rev-1): Black box with PiFace CAD LCD
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="spicam.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),3)
#
# RPi#3 (Rev-1): PiFace Digital
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="spicam.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),4)
#
# RPi#4 (Rev-1): 
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="spicam.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),6)
#
# RPi#6 (Rev-2 B+): OLD Kinshi filter control
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="rpisens.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),7)
#
# RPi#7 (Rev-2): PopKodi and Apache web server
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="popkodi.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): Watermeter Cam
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="rpiocr.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),9)
#
# RPi#9 (Rev-3): VPN Server
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="rpislim.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): SpiCam HTTP Server
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="spicam.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),11)
#
# RPi#11 (Rev-3B+): NEW Kinshi filter control (Buster+Overlay ro Fs)
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="rpisens.service"'
	BUPDIR=/all/rpi
endif

ifeq ($(BOARD),12)
#
# RPi#12 (Rev-4): Birdcage Cam
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="pikrellcam.service"'
	BUPDIR=/usr/local/share/rpi
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="spicam.service"'
	BUPDIR=/usr/local/share/rpi
endif

CC		   = gcc
CFLAGS	= -O2 -Wall -g
DEFS    += -D BOARD_RPI$(BOARD)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= -I$(COMMON)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS	   =
LIBCOM	= -lcommon
DEPS	   = config.h globals.h
OBJ		= rpi_main.o

debug: LIBCOM = -lcommondebug
debug: DEFS += -D FEATURE_USE_PRINTF -D DEBUG_ASSERT
debug: all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBCOM) $(LDFLGS)
	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/*.c ./
	cp -u /mnt/rpi/softw/$(TARGET)/*.h ./
	cp -u /mnt/rpi/softw/$(TARGET)/makefile ./
	cp -u /mnt/rpi/softw/$(TARGET)/rebuild ./ 2>/dev/null || :
	cp -u /mnt/rpi/softw/$(TARGET)/$(TARGET).service ./

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(TARGET)/*.c ./
	cp /mnt/rpi/softw/$(TARGET)/*.h ./
	cp /mnt/rpi/softw/$(TARGET)/makefile ./
	cp /mnt/rpi/softw/$(TARGET)/rebuild ./ 2>/dev/null || :
	cp /mnt/rpi/softw/$(TARGET)/$(TARGET).service ./

ziplog:
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo gzip -c $(BUPDIR)/$(TARGET).log 2>/dev/null >>$(BUPDIR)/$(TARGET)logs.gz
	sudo rm $(BUPDIR)/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

copylog:
	@echo "Copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cp /mnt/rpicache/$(TARGET).log /mnt/rpi/softw/$(TARGET)/$(TARGET).log 2>/dev/null || :

sortlog:
	@echo "Sort and copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cat /mnt/rpicache/$(TARGET).log | sort -n >/mnt/rpicache/$(TARGET)sort.log
	sudo cp /mnt/rpicache/$(TARGET)sort.log /mnt/rpi/softw/$(TARGET)/$(TARGET).log 2>/dev/null || :
	sudo rm /mnt/rpicache/$(TARGET)sort.log

dellog:
	@echo "Delete logfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm $(BUPDIR)/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

clean:
	rm -rf *.o
	rm -rf $(DESTDIR)/$(TARGET)

install:
	@echo "Install: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo systemctl stop $(TARGET).service
	cp $(DESTDIR)/$(TARGET) $(DESTDIR)/$(TARGET).latest 2>/dev/null || :
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	@echo `date`: New Install RPi-$(TARGET) Board=RPi#$(BOARD) >>$(BUPDIR)/$(TARGET).log
	sudo gzip -c $(BUPDIR)/$(TARGET).log 2>/dev/null >>$(BUPDIR)/$(TARGET)logs.gz
	sudo rm -rf $(BUPDIR)/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET).log
	sudo cp $(TARGET).service /lib/systemd/system/ 2>/dev/null || :
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET 2>/dev/null || :)
	sudo systemctl restart $(TARGET).service
	sudo systemctl daemon-reload

run:
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo systemctl restart $(TARGET).service

restart:
	sudo systemctl stop $(TARGET).service
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo systemctl restart $(TARGET).service

reload:
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET) 2>/dev/null || :
	sudo cp $(TARGET).service /lib/systemd/system/$(TARGET).service
	sudo systemctl enable $(TARGET).service
	sudo systemctl start $(TARGET).service

setup:
	sudo cp $(BUPDIR)/$(TARGET).log /mnt/rpicache/$(TARGET).log 2>/dev/null || :
	sudo cp $(TARGET).service /lib/systemd/system/$(TARGET).service

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
