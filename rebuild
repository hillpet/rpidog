#!/usr/bin/env bash
#################################################################################
# rebuild
#	Simple rebuild and install script
#
#	Copyright (c) 2017..2019 Patrn.nl
#################################################################################
# This file is part of rpidog:
#
# RpiDog is a simple guard dog monitor checking RpiSlim on restart and/or reboot.
#
# This script will rebuild and restart the RpiDog files and service in this case.
# To rebuild all RpiDog files, run:
#
# $ rpi-raptor
# 	 connect to //Raptor/Peter/proj/
# $	mkdir ~/proj
# $	cd ~/proj
# $	mkdir common
# $	mkdir rpidog
# $	cd rpidog
# $	mkdir bin
# $ mc 
# 	 copy from //Raptor/Rpi/softw/common:
# 		makefile
# 	 copy from //Raptor/Rpi/softw/rpidog:
# 		makefile
# $	cd ~
# $	./rebuild all
#
#################################################################################

check_make_ok() {
  if [ $? != 0 ]; then
    echo ""
    echo "Make Failed..."
    echo "Please check the messages and fix any problems."
    echo ""
	exit $?
  fi
}

if [ x$1 = "xdirs" ]; then
	if ! test -d proj
	then
	  mkdir proj
	  mkdir proj/common
	  mkdir proj/rpidog
	  mkdir proj/rpidog/bin
	fi
fi

if [ x$1 = "xinstall" ]; then
  echo
  echo "RpiDog install"
  echo "======================="
  sudo cp bin/rpidog /usr/sbin/rpidog
  check_make_ok
  sudo cp rpidog.service /lib/systemd/system/rpidog.service
  check_make_ok
  sudo systemctl enable rpidog.service
  check_make_ok
  sudo systemctl start rpidog.service
  check_make_ok
  echo "Rebuild done."
  echo ""
fi

if [ x$1 = "xall" ]; then
  echo
  echo "RpiDog Re-Build script"
  echo "======================="
  echo
  echo "Rebuild Common Library"
  cd ~/proj/common
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild RpiDog"
  cd ~/proj/rpidog
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild done."
  echo ""
fi

if [ x$1 = "xdebug" ]; then
  echo
  echo "RpiDog Re-Build script for DEBUG only!"
  echo "======================================="
  echo
  echo "Rebuild Common DEBUG Library"
  cd ~/proj/common
  make sync
  make clean
  make debug
  check_make_ok
  echo "Rebuild RpiDog"
  cd ~/proj/rpidog
  make sync
  make clean
  make debug
  check_make_ok
  echo "Rebuild DEBUG done."
  echo ""
fi

if [ x$1 = "xcommon" ]; then
  echo
  echo "common Re-Build script"
  echo "======================"
  echo
  echo "Rebuild Common Library"
  cd ~/proj/common
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild done."
  cd ~/proj/rpidog
  echo ""
fi

if [ x$1 = "x" ]; then
	echo ""
	echo "RpiDog rebuilder. Use to create all necessary files on a new or modified"
	echo "installation. Usage: $0 [all | debug | dirs | common | install]"
	echo ""
fi
