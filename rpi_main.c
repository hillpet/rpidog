/*  (c) Copyright:  2012..2019  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_main.c
 *  Purpose:            rpidog is a simple watchdog to monitor a running App
 *                      THE_APP is supplied through the makefile
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    17 Sep 2017:      Created
 *    15 Nov 2019:      Add SIGHUP for process heartbeat
 *    06 Feb 2024:      Move to printfx.h; Remove WAN checks here
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <errno.h>

#define USE_PRINTF
#include <printx.h>
#include <common.h>
#include "config.h"
//
#include "rpi_main.h"



#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

#ifdef   DEBUG_ASSERT
static   void rpi_Debug(void);
#define  RPI_DEAMON()                  rpi_Debug()

#else    //DEBUG_ASSERT
static   pid_t rpi_Deamon(void);
#define  RPI_DEAMON()                  rpi_Deamon()
#endif   //DEBUG_ASSERT

//
static bool    rpi_CheckHeartbeat      (int);
//
static void    rpi_PostSemaphore       (void);
static bool    rpi_SignalRegister      (sigset_t *);
static void    rpi_ReceiveSignalHup    (int);
static void    rpi_ReceiveSignalInt    (int);
static void    rpi_ReceiveSignalUser1  (int);
static void    rpi_ReceiveSignalUser2  (int);
static void    rpi_ReceiveSignalSegmnt (int);
static void    rpi_ReceiveSignalTerm   (int);
//
static const char *pcTheApp         = THE_APP;
static const char *pcShellRestore   = "cp " RPI_BACKUP_DIR RPI_PUBLIC_LOG_FILE " " RPI_PUBLIC_DIR;
static const char *pcShellBackup    = "cp " RPI_PUBLIC_DIR RPI_PUBLIC_LOG_FILE " " RPI_BACKUP_DIR;
static const char *pcLogfile        = RPI_PUBLIC_DIR RPI_PUBLIC_LOG_FILE;
//
static bool       fDebugMode        = FALSE;
static WDCMD      tWatchdogCommand  = WD_NONE;
static sem_t      tSemRpi;
static GLOG       stLog;
//
// Common lib needs support:
//
bool  GLOBAL_CheckDelete(char *p)   {return(FALSE);}
int   GLOBAL_GetTraceCode()         {return(0);}
int   GLOBAL_GetDebugMask()         {return(0);}
int   GLOBAL_GetMallocs()           {return(0);}
void  GLOBAL_PutMallocs(int i)      {}
GLOG *GLOBAL_GetLog()               {return(&stLog);}


/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   main
//  Purpose:    Main entry point for the Raspberry Pi monitor
//
//  Parms:      Commandline options
//  Returns:    Exit codes
//
int main(int argc, char **argv)
{
   bool        fRpiRunning=TRUE;
   int         iNoHeartbeats=0;
   int         iColdStarts=0;
   int         iBeatsOkees=0;
   u_int32     ulSecsNow, ulSecs24h, ulSecsHeartbeat;
   sigset_t    tBlockset;

   if(!LOG_Init())
   {
      LOG_printf("main(): LOG init ERROR");
      _exit(EXIT_CC_GEN_ERROR);
   }
   LOG_printf("%s:Build:%s" CRLF, VERSION, RPI_BUILT);
   //
   RPI_DEAMON();
   //
   // Deamon mode (if not in debug build):
   //    the HOST has exited already and we are now running in the background
   //
   system(pcShellRestore);
   //
   if( LOG_ReportOpenFile((char *) pcLogfile) == FALSE)
   {
      GEN_Printf("rpidog(): Error opening log-file %s" CRLF, pcLogfile);
   }
   else
   {
      LOG_printf("rpidog():Logfile is %s" CRLF, pcLogfile);
      LOG_Report(0, "RPI", "%s:Build:%s (%s)", VERSION, RPI_BUILT, RPI_IO_BOARD);
      LOG_Report(0, "RPI", "RpiDog is watching %s", pcTheApp);
   }
   //
   if(rpi_SignalRegister(&tBlockset) == FALSE) _exit(1);
   //
   if(sem_init(&tSemRpi, 1, 0) == -1) LOG_Report(errno, "RPI", "rpidog():sem_init");
   //
   // Set checktime at 01:00 each night.
   //
   ulSecsNow       = RTC_GetSystemSecs();
   ulSecsHeartbeat = ulSecsNow + SECS_HEARTBEAT_CHECK;
   ulSecs24h       = RTC_GetMidnight(ulSecsNow) + SECS_ONE_DAY + SECS_ONE_HOUR;
   PRINTF("rpidog():Now=%d, 24h=%d" CRLF, ulSecsNow, ulSecs24h);
   //
   while(fRpiRunning)
   {
      //
      // Wait timeout or until triggered by a signal
      //
      switch(GEN_WaitSemaphore(&tSemRpi, MSECS_ONE_MINUTE))
      {
         default:
         case 0:
            //
            // Somebody out there is trying to tell us something.
            //-----------------------------------------------------------
            //    o SIGHUP    WD_HEARTBEAT   Heartbeat
            //    o SIGTERM   WD_EXIT        Ctl-X
            //    o SIGINT    WD_EXIT        Ctl-C
            //    o SIGSEGV   WD_REBOOT      Segmentation fault
            //    o SIGUSR1   WD_RESTART     theAPP (restart the App)
            //    o SIGUSR2   WD_REBOOT      theAPP (reboot the system)
            //
            //    Check if we need to do something drastically by checking tWatchdogCommand
            //
            switch(tWatchdogCommand)
            {
               default:
                  LOG_Report(0, "DOG", "rpidog():Unknown command (%d)", tWatchdogCommand);
                  tWatchdogCommand = WD_NONE;
                  break;

               case WD_NONE:
                  LOG_Report(0, "DOG", "rpidog():No command (%d)", tWatchdogCommand);
                  break;

               case WD_EXIT:
                  LOG_Report(0, "DOG", "rpidog():EXIT");
                  fRpiRunning = FALSE;
                  break;

               case WD_HEARTBEAT:
                  iBeatsOkees++;
                  LOG_Report(0, "DOG", "main():Heartbeats from %s=%d", pcTheApp, iBeatsOkees);
                  PRINTF("main():Heartbeats from %s=%d" CRLF, pcTheApp, iBeatsOkees);
                  tWatchdogCommand = WD_NONE;
                  break;

               case WD_INSTART:
                  if(fDebugMode)
                  {
                     iBeatsOkees = 0;
                     LOG_Report(0, "DOG", "rpidog():Internal watchdog restart request in DEBUG Mode: IGNORE");
                     LOG_printf("rpidog():Internal watchdog restart request in DEBUG Mode: IGNORE" CRLF);
                  }
                  else
                  {
                     //
                     // We need to restart theApp ourselves since something is fishy
                     //
                     iBeatsOkees = 0;
                     iColdStarts++;
                     LOG_Report(0, "DOG", "rpidog():Internal watchdog restart");
                     LOG_Report(0, "DOG", "rpidog():Watchdog restarts %s %d times this day", pcTheApp, iColdStarts);
                     GEN_RunBatch("sudo systemctl stop "    THE_APP, "");
                     GEN_Sleep(MSECS_STOP_DELAY);
                     GEN_RunBatch("sudo systemctl restart " THE_APP, "");
                     GEN_Sleep(MSECS_STOP_DELAY);
                  }
                  tWatchdogCommand = WD_NONE;
                  break;

               case WD_RESTART:
                  if(fDebugMode)
                  {
                     iBeatsOkees = 0;
                     LOG_Report(0, "DOG", "rpidog():Restart requested in DEBUG Mode: IGNORE");
                     LOG_printf("rpidog():Restart request in DEBUG Mode: IGNORE" CRLF);
                  }
                  else
                  {
                     //
                     // theApp restart
                     //
                     iBeatsOkees = 0;
                     iColdStarts++;
                     LOG_Report(0, "DOG", "rpidog():Watchdog restart requested !!");
                     LOG_Report(0, "DOG", "rpidog():Watchdog restarts %s %d times this day", pcTheApp, iColdStarts);
                     GEN_RunBatch("sudo systemctl stop "    THE_APP, "");
                     GEN_Sleep(MSECS_STOP_DELAY);
                     GEN_RunBatch("sudo systemctl restart " THE_APP, "");
                     GEN_Sleep(MSECS_STOP_DELAY);
                  }
                  tWatchdogCommand = WD_NONE;
                  break;

               case WD_REBOOT:
                  if(fDebugMode)
                  {
                     tWatchdogCommand = WD_NONE;
                     LOG_printf("rpidog():Reboot request in DEBUG Mode: IGNORE" CRLF);
                  }
                  else
                  {
                     //
                     // System reboot (only in RELEASE mode)
                     //
                     tWatchdogCommand = WD_NONE;
                     LOG_Report(0, "DOG", "rpidog():Watchdog to reboot");
                     system("sudo shutdown -t 10 -F -r");
                     fRpiRunning = FALSE;
                  }
                  break;
            }
            break;

         case 1:
            //
            // 60 secs Timeout 
            //
            ulSecsNow = RTC_GetSystemSecs();
            PRINTF("rpidog():Now=%d, 24h=%d" CRLF, ulSecsNow, ulSecs24h);
            if(ulSecsNow > ulSecsHeartbeat)
            {
               //
               // One hour passed: We should have heartbeats from theApp
               //
               if(!rpi_CheckHeartbeat(iBeatsOkees) ) 
               {
                  LOG_Report(0, "RPI", "rpidog():NO heartbeat from %s", pcTheApp);
                  iNoHeartbeats++;
               }
               ulSecsHeartbeat = ulSecsNow + SECS_HEARTBEAT_CHECK;
            }
            if(ulSecsNow > ulSecs24h)
            {
               //
               // One day has passed :
               //    - Check if heartbeat went AWOL (restart theApp)
               //
               PRINTF("rpidog():NEW DAY: Now=%d, 24h=%d" CRLF, ulSecsNow, ulSecs24h);
               if(iNoHeartbeats > MAX_MISSED_HEARTBEATS)
               {
                  LOG_Report(0, "RPI", "rpidog():%d missed heartbeats: Restart %s", iNoHeartbeats, pcTheApp);
                  //
                  tWatchdogCommand = WD_INSTART;
                  rpi_PostSemaphore();
               }
               PRINTF("rpidog():New day: App-resets(%d) App-Signals(%d)" CRLF, iColdStarts, iBeatsOkees);
               LOG_Report(0, "DOG", "rpidog():New day: App-resets(%d) App-Signals(%d)", iColdStarts, iBeatsOkees);
               //
               // Reset forced reboot count on multiple theAPP restarts per day after 24 hours
               //
               iNoHeartbeats = 0;
               iColdStarts   = 0;
               ulSecs24h    += SECS_ONE_DAY;
            }
            else
            {
               if(iColdStarts > MAX_RESTARTS)
               {
                  //
                  // Max restarts on a single day reached: reboot
                  //   
                  LOG_Report(0, "DOG", "rpidog():Max restarts reached: reboot...");
                  tWatchdogCommand = WD_REBOOT;
                  rpi_PostSemaphore();
               }
            }
            break;

         case -1:
            // error
            LOG_Report(0, "DOG", "rpidog():Sem error");
            break;
      }
   }
   if( sem_destroy(&tSemRpi) == -1) LOG_Report(errno, "RPI", "rpidog():sem_destroy");
   //
   LOG_Report(0, "DOG", "Version=%s now EXIT", VERSION);
   LOG_ReportCloseFile();
   //
   // Save the logfile
   //
   system(pcShellBackup);
   LOG_printf("rpidog():Exit" CRLF);
   _exit(0);
}

/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/

#ifdef  DEBUG_ASSERT
//
//  Function:   rpi_Debug
//  Purpose:    Watchdog debug mode
//
//  Parms:
//  Returns:
//
static void rpi_Debug(void)
{
   LOG_printf("rpi-Debug():Debug mode !" CRLF);
   fDebugMode = TRUE;
}

#else
//
//  Function:   rpi_Deamon
//  Purpose:    Deamonize the watchdog
//
//  Parms:
//  Returns:
//
static pid_t rpi_Deamon(void)
{
   pid_t    tPid;

   fDebugMode = FALSE;
   //
   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         PRINTF("rpi-Deamon(): Parent TERMINATES now." CRLF);
         _exit(0);
         break;

      case 0:
         // child
         PRINTF("rpi-Deamon(): Watchdog running background mode" CRLF);
         break;

      case -1:
         // Error
         _exit(1);
         break;
   }
   return(tPid);
}
#endif   //DEBUG_ASSERT

//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGHUP:
  if( signal(SIGHUP, &rpi_ReceiveSignalHup) == SIG_ERR)
  {
     LOG_Report(errno, "DOG", "rpi_SignalRegister(): SIGHUP ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "DOG", "rpi_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &rpi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "DOG", "rpi_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "DOG", "rpi_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "DOG", "rpi_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &rpi_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "DOG", "rpi_ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGHUP);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "DOG", "rpi_ReceiveSignalTerm()");
   //
   tWatchdogCommand = WD_EXIT;
   rpi_PostSemaphore();
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "DOG", "rpi_ReceiveSignalInt()");
   //
   tWatchdogCommand = WD_EXIT;
   rpi_PostSemaphore();
}

//
//  Function:   rpi_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalSegmnt(int iSignal)
{
   LOG_Report(errno, "DOG", "rpi_ReceiveSignalSegmnt()");
   tWatchdogCommand = WD_REBOOT;
   rpi_PostSemaphore();
}

//
// Function:   rpi_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to signal theApp restart
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   LOG_Report(0, "DOG", "rpi_ReceiveSignalUser1()");
   tWatchdogCommand = WD_RESTART;
   rpi_PostSemaphore();
}

//
// Function:   rpi_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2 is used to signal system reboot
//
static void rpi_ReceiveSignalUser2(int iSignal)
{
   LOG_Report(0, "DOG", "rpi_ReceiveSignalUser2()");
   tWatchdogCommand = WD_REBOOT;
   rpi_PostSemaphore();
}

//
// Function:   rpi_ReceiveSignalHup
// Purpose:    System callback on receiving the SIGHUP signal
//
// Parms:
// Returns:    
// Note:       SIGHUP is used to signal system heartbeat
//
static void rpi_ReceiveSignalHup(int iSignal)
{
   tWatchdogCommand = WD_HEARTBEAT;
   rpi_PostSemaphore();
}


/* ======   Local Functions separator ===========================================
___MISC_FUNCTIONS(){}
==============================================================================*/

//
// Function:   rpi_PostSemaphore
// Purpose:    Unlock the watchdog Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void rpi_PostSemaphore()
{
   //
   // sem++ (UNLOCK)   
   //
   if(sem_post(&tSemRpi) == -1) LOG_Report(errno, "DOG", "rpi_PostSemaphore(): sem_post error");
}

//
// Function:   rpi_CheckHeartbeat
// Purpose:    Check if theApp is still beating
//
// Parms:      Heartbeats now
// Returns:    TRUE if OK heartbeat
// Note:       
//
static bool rpi_CheckHeartbeat(int iBeats)
{
   static int  iOldBeats=0;
   //
   bool  fCc=TRUE;

   //
   // We MUST have at least one single heartbeat from theApp to start this check !
   //
   if(iBeats)
   {
      PRINTF("rpi-CheckHeartbeat():Heartbeats went from %d to %d" CRLF, iOldBeats, iBeats);
      if(iOldBeats == 0) 
      {
         //
         // Initial situation: start heartbeat checks
         //
         LOG_Report(0, "RPI", "rpi-CheckHeartbeat():Started checking heartbeat(%d) from %s", iBeats, pcTheApp);
         iOldBeats = iBeats;
      }
      else if(iOldBeats == iBeats)
      {
         //
         // No heartbeats during the last hour !
         // Problems with theApp heartbeat: restart theApp at the end of the day
         //
         fCc = FALSE;
      }
      else iOldBeats = iBeats;
   }
   return(fCc);
}

