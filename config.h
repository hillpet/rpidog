/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           config.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *                     
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       17 Sep 2017
 * 
 *  Revisions:
 *
 *
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//
// RELEASE defines /home/public
//
#define VERSION                     "Dog-v105.014"
#define RPI_PUBLIC_DIR              "/mnt/rpicache/"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
#define RPI_PUBLIC_LOG_FILE         "rpidog.log"
//
#define RPI_IO_BOARD                "RpiDog uses NO IO !"
#define MAX_RESTARTS                5

#endif /* _CONFIG_H_ */
