/*  (c) Copyright:  2012..2019  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_main.h
 *  Purpose:            rpidog is a simple watchdog tp control a HTTP server
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    17 Sep 2017       Created
 *    15 Nov 2019       Add SIGHUP for process heartbeat
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIN_H_
#define _RPI_MAIN_H_

#define  MSECS_STOP_DELAY           2000
#define  MSECS_ONE_MINUTE           60000
//
#define  SECS_ONE_MINUTE            (25*60*60)
#define  SECS_ONE_HOUR              (60*60)
#define  SECS_ONE_DAY               (24*60*60)
//
#define  SECS_HEARTBEAT_CHECK       3600
#define  MAX_MISSED_HEARTBEATS      5
//
typedef enum __wd_command__
{
   WD_NONE = 0,
   WD_HEARTBEAT,
   WD_RESTART,
   WD_REBOOT,
   WD_EXIT,
   WD_INSTART,
   //
   NUM_WD_CMDS
}  WDCMD;


#endif /* _RPI_MAIN_H_ */
